<?php
require_once('../model/Cliente.php');
require_once('../model/ClientModel.php');
require_once ('../model/CuentaModel.php');

use Cliente;

require_once ('../helpers/validations.php');

if ( isset($_POST['submit']) ) {
    if ($_POST['control'] == 'register') {
        //$_POST['message']=validate();
        if (validate()) {
            $cliente = new Cliente($_POST['userName'], $_POST['userSurname'], $_POST['birthdate'], $_POST['genre'], $_POST['mobileNumber'], $_POST['dni'], $_POST['email'], $_POST['pass'], "");
            insertCliente($cliente);
            header('Location: ../views/login.php');
        } else {
            require_once('../views/register.php');
        }
    } elseif ($_POST['control'] == 'login') {
        error_log("login");
        $hash = getUserHash($_POST['dni']);
        error_log("$hash");
        if (password_verify($_POST['password'], $hash)) {
            error_log("pass ok");
            session_start();
            $_SESSION['user'] = $_POST['dni'];
            header('Location: ../views/welcome.php');
        } else {
            require_once('../views/login.php');
        }
    }

    if ($_POST['control'] == 'profile') {
        echo "Entro";
        $check = getimagesize($_FILES['upload']['tmp_name']);
        $fileName = $_FILES['upload']['name'];
        $fileSize = $_FILES['upload']['size'];
        $fileType = $_FILES['upload']['type'];
        echo $fileName . '<br/>';
        echo $fileSize . '<br/>';
        echo $fileType . '<br/>';
        if ($check !== false) {
            $image = file_get_contents($_FILES['upload']['tmp_name']);
            updateCliente($image);
        }
        // Vamos a mostrar la imagen
        $obj = selectCliente();
        ob_start();
        fpassthru($obj->getImage());
        $data = ob_get_contents();
        ob_end_clean();
        $img = "data:image/*;base64," . base64_encode($data);
        echo "<img src=" . $img . "/>";
    }

    if ($_POST['control'] == 'create') {
        echo "entroo";
        session_start();
        echo "entroo" . $_SESSION['user'];
        createAccount($_SESSION['user']);
        header('Location: ../views/welcome.php');
    }

    if ($_POST['control'] == 'select_account') {
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo'] = $saldo;
        header("Location: query.php");
    }
}
?>